import { hot } from 'react-hot-loader/root';
import React from 'react';
import GlobalStyle from '../theme';
import { Application } from './styles';
import Dashboard from './components/dashboard/Dashboard';
import ViewShip from './components/view-ship/ViewShip';
import { useSelector } from 'react-redux';

const App = () => {
  const ship = useSelector(state => state.view);

  return <>
    <Application >
      {
        ship != null ? <ViewShip /> : <Dashboard />
      }
    </Application>
    <GlobalStyle />
  </>;
};

export default hot(App);