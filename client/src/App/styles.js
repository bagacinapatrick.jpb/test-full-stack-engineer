import styled from 'styled-components';

const Application = styled.div`
  font-family: Roboto;
  font-weight: 300;
  font-size: 20px;
  font-style: italic;
  color: white;
  position: absolute;
  padding: 50px;
  svg, span {
      padding-left: 10px;
  }
`;

const SearchStyle = styled.div`
  div {
    margin-bottom: 15px;
  }

  label, input {
    margin-right: 15px;
  }
`;

const TableStyle = styled.div`
  table {
    border-collapse: collapse;
    width: 100%;
  }

  th {
    background-color: #4c7093;
  }

  tr:hover {
    background-color: #4c7093;
    cursor: pointer;
  }

  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
`;

export {
  Application,
  SearchStyle,
  TableStyle
};