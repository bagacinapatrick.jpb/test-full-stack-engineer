import React from 'react';
import Table from '../table/Table';
import Search from '../search/Search';

const Dashboard = () => {
  return <>
    <h1>Dashboard</h1>
    <Search />
    <Table />
  </>;
}

export default Dashboard;