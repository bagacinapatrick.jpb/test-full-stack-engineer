import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { unselectShip } from '../../../redux/actions';

const ViewShip = () => {
  const {
    ship_name,
    ship_type,
    weight_kg,
    home_port,
    icon,
  } = useSelector(state => state.view);
  const dispatch = useDispatch();

  return <>
    <div>
      <button onClick={() => dispatch(unselectShip())}>Go Back</button>
      <h1>{ship_name}</h1>
    </div>
    <div>
      <label>Type: {ship_type}</label>
    </div>
    <div>
      <label>Weight: {weight_kg ? weight_kg : '--'}</label>
    </div>
    <div>
      <label>Home Port: {home_port}</label>
    </div>
    <div>
      {
        icon ? <img src={icon} alt="Icon" height="100" width="100" /> : <label>No icon uploaded yet.</label>
      }
    </div>
  </>;
}

export default ViewShip;