import React, { useState } from 'react';
import { SearchStyle } from '../../styles';
import api from '../../../api';
import { useDispatch, useSelector } from 'react-redux';
import { updateResult, search } from '../../../redux/actions';

const Search = () => {
  const page = useSelector(state => state.search.page);
  const dispatch = useDispatch();

  const [shipType, setShipType] = useState(null);
  const [weight, setWeight] = useState(null);
  const [homePort, setHomePort] = useState(null);

  const applySearch = () => {
    const q = {
      ship_type: shipType,
      weight: weight,
      home_port: homePort,
    };
    page.offset = 0;
    
    refresh(q, page);
    dispatch(search(q));
  }

  const refresh = async (q, p) => {
    const result = await api.listShips({...q, ...p});
  
    if (result.status === 200) {
      const res = await result.json();
  
      dispatch(updateResult(res));
    }
  }

  return <>
    <SearchStyle>
      <div>
        <label>Ship Type</label>
        <input type="text" id="shipType" name="shipType" onChange={(e) => setShipType(e.target.value)} />
      </div>
      <div>
        <label>Weight</label>
        <input type="text" id="weight" name="weight" onChange={(e) => setWeight(e.target.value)} />
      </div>
      <div>
        <label>Home Port</label>
        <input type="text" id="homePort" name="homePort" onChange={(e) => setHomePort(e.target.value)} />
        <button onClick={applySearch}>Search</button>
      </div>
    </SearchStyle>
  </>;
}

export default Search;