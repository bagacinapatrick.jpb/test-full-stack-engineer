import React, { useState } from 'react';
import { TableStyle } from '../../styles';
import Pagination from './Pagination';
import { useSelector, useDispatch } from 'react-redux';
import { selectShip } from '../../../redux/actions';
import api from '../../../api';

const Table = () => {
  const ships = useSelector(state => state.search.result);
  const dispatch = useDispatch();

  const [upload, setUpload] = useState(null);
  const [activeShip, setActive] = useState(null);
  
  const fileTypes = ['image/png', 'image/jpeg', 'image/jpg'];

  const onSelect = async (id) => {
    const result = await api.getShip(id);
  
    if (result.status === 200) {
      const res = await result.json();
  
      dispatch(selectShip(res));
    }
  }

  const onFileChange = (e) => {
    e.stopPropagation();
    e.preventDefault();
    const file = e.target.files[0];
    const size_kb = file.size * 0.001;
  
    if (size_kb > 100) {
      alert('Please upload a file upto size of 100kb');
      return;
    }

    if (!fileTypes.includes(file.type)) {
      alert('Invalid file type');
      return;
    }

     uploadFile(file);
  }

  const uploadFile = async (file) => {
    const base64 = await fileToBase64(file);
    const res = await api.uploadIcon({
      icon: base64,
      id: activeShip.ship_id,
    });
  
    if (res.status === 200) {
      alert(`Successfully uploaded an icon for ${activeShip.ship_name}. \nPlease click the ship to view the uploaded image.`);
    } else if (res.status === 400) {
      const body = await res.json();
      alert(body.message);
    } else {
      alert('Unable to upload the image. Please try again.')
    }
  }

  const fileToBase64 = (file) => {
    return new Promise(resolve => {
      var reader = new FileReader();
      // Read file content on file loaded event
      reader.onload = function(event) {
        resolve(event.target.result);
      };
      
      // Convert data to base64 
      reader.readAsDataURL(file);
    });
  };

  return <>
    <TableStyle>
      <input id="myInput"
        type="file"
        ref={(ref) => setUpload(ref)}
        style={{display: 'none'}}
        accept={fileTypes.join(', ')}
        onChange={onFileChange}
      />
      <table>
        <thead>
          <tr>
            <th>Ship Type</th>
            <th>Weight</th>
            <th>Home Port</th>
            <th>Ship Name</th>
            <th>Class</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {
            ships.map(s => 
              <tr key={s.ship_id} onClick={() => onSelect(s.ship_id)}>
                <td>{s.ship_type}</td>
                <td>{s.weight_kg}</td>
                <td>{s.home_port}</td>
                <td>{s.ship_name}</td>
                <td>{s.class}</td>
                <td>
                  <button onClick={
                    (e) => {
                      e.stopPropagation();
                      setActive(s);
                      upload.click();
                    }
                  }>Upload Icon</button>
                </td>
              </tr>
            )
          }
        </tbody>
      </table>
      {
        ships.length > 0 && <Pagination />
      }
    </TableStyle>
  </>;
}

export default Table;