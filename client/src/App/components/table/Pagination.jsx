import React from 'react';
import api from '../../../api';
import { useDispatch, useSelector } from 'react-redux';
import { updateResult, previousPage, nextPage } from '../../../redux/actions';

const Pagination = () => {
  const page = useSelector(state => state.search.page);
  const query = useSelector(state => state.search.query);
  const total = useSelector(state => state.search.result.length);
  const dispatch = useDispatch();

  const prev = () => {
    dispatch(previousPage());
    refresh();
  }

  const next = () => {
    dispatch(nextPage());
    refresh();
  }

  const refresh = async () => {
    const result = await api.listShips({...query, ...page});
  
    if (result.status === 200) {
      const res = await result.json();
  
      dispatch(updateResult(res));
    }
  }

  return <>
    <div>
      <button onClick={prev} disabled={page.offset === 0}>Previous</button>
      <button onClick={next} disabled={total < page.limit}>Next</button>
    </div>
  </>;
}

export default Pagination;