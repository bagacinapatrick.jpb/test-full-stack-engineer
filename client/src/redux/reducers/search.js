const initial = {
  result: [],
  query: {
    ship_type: null,
    weight: null,
    home_port: null,
  },
  page: {
    limit: 10,
    offset: 0,
    sort: 'ship_name',
    order: 'asc',
  },
};

const searchReducer = (state = initial, action) => {
  const { type, payload } = action;
  switch (type) {
    case 'SEARCH':
      state.query = payload;
      state.page.offset = 0;
      break;

    case 'PAGE_NEXT':
      state.page.offset += state.page.limit;
      break

    case 'PAGE_PREVIOUS':
      if (state.page.offset > 0) state.page.offset -= state.page.limit;
      break;

    case 'UPDATE_RESULT':
      state.result = payload
      break;

    default:
  }

  return state;
}

export default searchReducer;