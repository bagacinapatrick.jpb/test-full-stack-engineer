const viewShipReducer = (state = null, action) => {
  const { type, payload } = action;
  switch (type) {
    case 'SELECT_SHIP':
      return state = payload;

    case 'UNSELECT_SHIP':
      return state = null;

    default:
      return state;
  }
}

export default viewShipReducer;