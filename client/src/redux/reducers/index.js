import searchReducer from './search';
import viewShipReducer from './view';
import { combineReducers } from 'redux';

const allReducers = combineReducers({
  search: searchReducer,
  view: viewShipReducer,
});

export default allReducers;