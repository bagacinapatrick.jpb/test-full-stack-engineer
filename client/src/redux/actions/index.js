export const search = (filter) => {
  return {
    type: 'SEARCH',
    payload: filter,
  }
}

export const updateResult = (result) => {
  return {
    type: 'UPDATE_RESULT',
    payload: result,
  }
}

export const previousPage = () => {
  return {
    type: 'PAGE_PREVIOUS',
  }
}

export const nextPage = () => {
  return {
    type: 'PAGE_NEXT',
  }
}

export const selectShip = (ship) => {
  return {
    type: 'SELECT_SHIP',
    payload: ship,
  }
}

export const unselectShip = () => {
  return {
    type: 'UNSELECT_SHIP'
  }
}