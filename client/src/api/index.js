import config from '../config.json';

export default class SpaceXAPI {
  static async getShip(id) {
    const result = await fetch(`${config.server.baseUrl}/ships/${id}`);

    return result;
  }

  static async listShips(query) {
    let params = [];

    if (query.ship_type) params.push(`ship_type=${query.ship_type}`);
    if (query.weight) params.push(`weight=${query.weight}`);
    if (query.home_port) params.push(`home_port=${query.home_port}`);
    if (query.limit) params.push(`limit=${query.limit}`);
    if (query.offset != null) params.push(`offset=${query.offset}`);
    if (query.sort) params.push(`sort=${query.sort}`);
    if (query.order) params.push(`order=${query.order}`);
    
    const result = await fetch(`${config.server.baseUrl}/ships?${params.join('&')}`);

    return result;
  }

  static async uploadIcon(cmd) {
    const result = fetch(`${config.server.baseUrl}/ships/upload`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(cmd),
    });

    return result;
  }
}