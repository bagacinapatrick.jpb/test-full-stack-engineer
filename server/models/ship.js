const dbPool = require('../db');
const api = require('../api/spacex-api');
const ex = require('../api/exceptions');
class Ship {
  constructor() {
    this.table = 'spaceData';
  }

  async get(id) {
    const rows = await dbPool.query(`SELECT * FROM ${this.table} WHERE id = '${id}'`);
    let res;
    
    if (rows && rows.length > 0) {
      console.log('Getting data from local database...');
      res = JSON.parse(rows[0].spaceItem);
      res.icon = rows[0].icon;
    }
    else {
      res = await api.getShip(id);

      if (!res) return res;

      // save to local DB
      console.log('Saving data to local database...');
      await dbPool.query(`
        INSERT INTO ${this.table} (id, spaceItem)
        VALUES ('${res.ship_id}', '${JSON.stringify(res)}')
      `);
    }

    return res;
  }

  async list(query, opts) {
    const res = await api.listShips(query, opts);

    if (res && res.length === 0) return [];

    // filter data that are not yet saved to local db
    const savedIds = (
      await dbPool.query(`
        SELECT id FROM ${this.table} 
        WHERE id IN ('${
          res.map(r => r.ship_id).join('\', \'')
        }')
      `)
    ).map(r => r.id);

    const unsaved = await res.filter(r => !savedIds.includes(r.ship_id));
    if (unsaved && unsaved.length > 0) {
      console.log('Saving results to local database...');
      let q = `INSERT INTO ${this.table} (id, spaceItem) VALUES `;
      const values = unsaved.map((item, index) => `('${item.ship_id}', '${JSON.stringify(item)}')${index === (unsaved.length - 1) ? ';' : ','}`)
      const fq = q + values.join(' ');
      
      await dbPool.query(fq);
    }

    return res;
  }

  async uploadIcon(cmd) {
    console.log('Uploading......');
    const rows = await dbPool.query(`SELECT * FROM ${this.table} WHERE id = '${cmd.id}'`);

    if (rows && rows.length === 0) throw new ex.ShipNotFoundException();

    await dbPool.query(
      `UPDATE ${this.table} SET icon = '${cmd.icon}' WHERE id = '${cmd.id}'`
    );

    return JSON.parse(rows[0].spaceItem);
  }

  async reset() {
    await dbPool.query(`DELETE FROM ${this.table}`);
    const rows = await dbPool.query(`SELECT * FROM ${this.table}`);
  
    console.log(`All rows after reset: ${rows}`);
  }
}

module.exports = new Ship();