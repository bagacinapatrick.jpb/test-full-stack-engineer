const ship = require('../models/ship');
const ex = require('../api/exceptions');

class ShipService {
  get(id) {
    return ship.get(id);
  }

  list(query, opts) {
    return ship.list(query, opts);
  }

  uploadIcon(cmd) {
    if (!cmd.id) throw new ex.ValidationException('ID is required');
    if (!cmd.icon) throw new ex.ValidationException('Icon is required');

    return ship.uploadIcon(cmd);
  }

  reset() {
    return ship.reset();
  }
}

module.exports = new ShipService();