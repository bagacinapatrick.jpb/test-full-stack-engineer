const rp = require('request-promise');
const config = require('../config.json');

class SpaceXApi {
  async getShip(id) {
    var op = {
      uri: `${config['api']['baseUrl']}/ships/${id}`,
      json: true,
    };
    
    try {
      const res = await rp(op);
      
      return res;
    } catch (err) {
      if (err.statusCode === 404) return null;
      
      // TODO: Handle error
      console.log(err);
    }
  }

  async listShips(query, opts) {
    var op = {
      uri: `${config['api']['baseUrl']}/ships`,
      json: true,
    };

    if (query.ship_type || query.weight || query.home_port || opts.limit || opts.offset || opts.sort || opts.order) {
      var params = {};

      // query params
      if (query.ship_type) params.ship_type = query.ship_type;
      if (query.weight) params.weight_kg = query.weight;
      if (query.home_port) params.home_port = query.home_port;

      // paging params
      if (opts.limit) params.limit = opts.limit;
      if (opts.offset) params.offset = opts.offset;
      if (opts.sort) params.sort = opts.sort;
      if (opts.order) params.order = opts.order;

      op.qs = params;
    }
    
    try {
      const res = await rp(op);
      return res;
    } catch (err) {
      // TODO: Handle error
      console.log(err);
    }
  }
}

module.exports = new SpaceXApi();