class ShipNotFoundException extends Error {
  constructor() {
    super('Ship not found');
    this.name = 'SHIP_NOT_FOUND';

    console.error(this.message);
  }
}

class ValidationException extends Error {
  constructor(message) {
    super(message);
    this.name = 'VALIDATION_EXCEPTION';

    console.error(message);
  }
}

module.exports = {
  ShipNotFoundException,
  ValidationException
};

