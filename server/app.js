const express =require('express');
const bodyParser = require('body-parser');
const shipSvc = require('./service/ship-service');
const cron = require('node-cron');

const app = express();

cron.schedule('* 23 * * *', () => {
  console.log('Cleaning local database....');
  shipSvc.reset();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

// set headers to allow CORS request
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

app.listen('4000');
console.log(`Listening on port: 4000, wait for the development server to be up...`);

// set routes
const routes = require('./controllers/routes');
app.use(routes);