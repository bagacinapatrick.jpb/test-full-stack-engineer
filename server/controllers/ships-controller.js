const ship = require('../models/ship');
const shipSvc = require('../service/ship-service');
const ex = require('../api/exceptions');

const get = (req, res) => {
  shipSvc.get(req.params.id)
    .then((ship) => {
      if (!ship) {
        res
          .status(400)
          .json({
            message: 'Ship Not Found',
          });
      } else {
        res
          .status(200)
          .cache(ship);
      }
    })
    .catch((err) => {
      console.log(err);
      res
        .status(400)
        .json({
          message: 'Bad request'
        });
    });
}

const list = (req, res) => {
  const query = {
    ship_type: req.query.ship_type,
    weight: req.query.weight,
    home_port: req.query.home_port,
  };
  const opts = {
    limit: req.query.limit,
    offset: req.query.offset,
    sort: req.query.sort,
    order: req.query.order,
  };
  
  shipSvc.list(query, opts)
    .then((ships) => {
      res
      .status(200)
      .cache(ships);
    })
    .catch((err) => {
      console.log(err);
      res
        .status(400)
        .json({
          message: 'Bad request'
        });
    });
}

const upload = (req, res) => {
  try {
    shipSvc.uploadIcon(req.body)
    .then((result) => {
      res
        .status(200)
        .json(result);
    }).catch((err) => {
      if (err instanceof ex.ShipNotFoundException) {
        res
          .status(400)
          .json({
            message: err.message,
            name: err.name,
          });
      } else {
        console.log(err);
        res
          .status(400)
          .json({
            message: 'Bad request'
          });
      }
    });
  } catch (err) {
    if (err instanceof ex.ValidationException) {
      res
        .status(400)
        .json({
          message: err.message,
          name: err.name,
        });
    } else {
      res
        .status(500)
        .json({
          name: 'SERVER_ERROR',
        });
    }
  }
}

module.exports = {
  get,
  list,
  upload,
};