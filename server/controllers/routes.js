const express = require('express'),
  router = express.Router(),
  mcache = require('memory-cache');

const CACHE_DURATION = 30;
const cache = (duration) => {
  return (req, res, next) => {
    const key = req.url;
    const cachedResponse = mcache.get(key);

    if (cachedResponse) {
      console.log(`Responding from cached ${key}...`);
      res
        .status(200)
        .json(cachedResponse);
      return;
    } else {
      console.log(`Caching response for ${key}...`);
      res.cache = (body) => {
        mcache.put(key, body, duration * 1000);
        res.json(body);
      }
      next();
    }
  }
}

// controllers
const shipsCtrl = require('./ships-controller');

// ships routes
router.get('/ships', cache(CACHE_DURATION), shipsCtrl.list);
router.get('/ships/:id', cache(CACHE_DURATION), shipsCtrl.get);

router.post('/ships/upload', shipsCtrl.upload);

module.exports = router;